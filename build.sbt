name := "finance-log"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies ++= Seq(
  // for config reading
  "com.github.pureconfig" %% "pureconfig" % "0.17.1", // обновилось

  // main effect
  "dev.zio" %% "zio"                 % "1.0.15", // обновилось
  "dev.zio" %% "zio-interop-cats"    % "2.5.1.0", // 2.5.1.0 - последняя с поддержкой cats 2.x

  // default lib for happy being
  "org.typelevel" %% "cats-core" % "2.7.0", // обновилось
  "org.typelevel" %% "cats-tagless-macros" %  "0.14.0",

  // for http client
  "com.softwaremill.sttp.client" %% "core"            % "2.3.0", // обновилось
  "com.softwaremill.sttp.client" %% "async-http-client-backend-cats" % "2.3.0", // обновилось

  //  for http server
  "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % "0.17.19", // тапир не обновляется
  "com.softwaremill.sttp.tapir" %% "tapir-json-tethys" % "0.17.19",
  "com.softwaremill.sttp.tapir" %% "tapir-newtype" % "0.17.19",

  "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-http4s" % "0.17.19",  //  не обновилось  :(
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs"        % "0.17.19", //  не обновилось  :(
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml"  % "0.17.19", //  не обновилось  :(

  // json
  "com.tethys-json" %% "tethys-core" % "0.26.0",
  "com.tethys-json" %% "tethys-jackson" % "0.26.0",
  "com.tethys-json" %% "tethys-derivation" % "0.26.0",
  "com.tethys-json" %% "tethys-enumeratum" % "0.26.0",

  // newtype
  "io.estatico" %% "newtype" % "0.4.4",
  // enumerantum
  "com.beachape" %% "enumeratum" % "1.7.0", // обновилось

  "tf.tofu" %% "derevo-tethys" % "0.13.0", // обновилось


  "org.slf4j" % "slf4j-api" % "1.7.36", // обновилось
  "ch.qos.logback" % "logback-classic" % "1.2.11", // обновилось
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5", // обновилось

  // for DB
  "org.tpolecat" %% "doobie-core"      % "0.13.4", // обновилось
  "org.tpolecat" %% "doobie-postgres"  % "0.13.4",
  "org.tpolecat" %% "doobie-h2"        % "0.13.4",          // H2 driver 1.4.200 + type mappings.
  "org.tpolecat" %% "doobie-hikari"    % "0.13.4",          // HikariCP transactor.
  "org.tpolecat" %% "doobie-specs2"    % "0.13.4" % "test", // Specs2 support for typechecking statements.
  "org.tpolecat" %% "doobie-scalatest" % "0.13.4" % "test"  // ScalaTest support for typechecking statements.

)

scalacOptions := Seq(
  "-Ymacro-annotations"
)
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
addCompilerPlugin("org.typelevel" % "kind-projector" % "0.11.3" cross CrossVersion.full)
