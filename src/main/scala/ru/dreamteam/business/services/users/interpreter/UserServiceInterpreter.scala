package ru.dreamteam.business.services.users.interpreter

import cats.effect.Sync
import cats.syntax.all._
import ru.dreamteam.business.repository.users.UsersRepository
import ru.dreamteam.business.repository.users.UsersRepository.UserReq
import ru.dreamteam.business.services.session.SessionService
import ru.dreamteam.business.services.users.UserService
import ru.dreamteam.business.{Token, User}

import java.util.NoSuchElementException

class UserServiceInterpreter[F[_] : Sync](userRepo: UsersRepository[F], sessionService: SessionService[F])
  extends UserService[F] {

  // TODO: придумать нормальное решение с выбрасываемыми ошибками, чтобы кидать типы ошибок, а что-то подставляло им текст на вывод, перенести в хендлеры
  // TODO: Добавить проверку на правильность пароля
  override def login(login: User.Login, password: User.Password): F[Token] =
    (for {
      user <- userRepo.findUserByLogin(login)
      token <- sessionService.generate(user.get)
    } yield token)
      .adaptErr({ case err: NoSuchElementException => new Exception("user not found") }) // <- это

  override def registration(login: User.Login, password: User.Password): F[User] =
    for {
      id <- userRepo.addUser(UserReq(login, password))
      user <- userRepo.findUser(id)
    } yield user.get
}
