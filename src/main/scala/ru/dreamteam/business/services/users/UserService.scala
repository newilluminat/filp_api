package ru.dreamteam.business.services.users

import ru.dreamteam.business._

trait UserService[F[_]] {
  def login(login: User.Login, password: User.Password): F[Token]

  def registration(
    login: User.Login,
    password: User.Password
  ): F[User]

}
