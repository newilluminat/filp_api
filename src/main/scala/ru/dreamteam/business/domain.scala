package ru.dreamteam.business

import derevo.derive
import derevo.tethys.{tethysReader, tethysWriter}
import enumeratum.{Enum, EnumEntry}
import io.estatico.newtype.macros.newtype
import sttp.tapir.codec.newtype._
import ru.dreamteam.infrastructure.newtype._
import tethys.enumeratum.TethysEnum

@derive(tethysReader, tethysWriter)
case class Token(token: String)

@derive(tethysReader, tethysWriter)
case class User(userId: User.Id, login: User.Login, password: User.Password)

object User {
  @newtype case class Id(id: String)
  @newtype case class Login(login: String)
  @newtype case class Password(password: String)
}
