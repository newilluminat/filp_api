package ru.dreamteam.business.repository

import cats.effect.{Resource, Sync}
import doobie.util.transactor.Transactor
import ru.dreamteam.business.repository.users.UsersRepository
import ru.dreamteam.business.repository.users.interpreter.UsersRepositoryInterpreter
import ru.dreamteam.infrastructure.utils.SHA256HashImpl

case class RepositoriesComponent[F[_]](userRepo: UsersRepository[F])

object RepositoriesComponent {
  def build[F[_]: Sync](transactor: Transactor[F]): Resource[F, RepositoriesComponent[F]] = {

    val hashGenerator = new SHA256HashImpl[F]
    val userRepo = new UsersRepositoryInterpreter[F](transactor, hashGenerator)

    Resource.pure(RepositoriesComponent[F](userRepo))
  }
}
