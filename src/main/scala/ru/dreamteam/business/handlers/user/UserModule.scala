package ru.dreamteam.business.handlers.user

import org.http4s.HttpRoutes
import ru.dreamteam.infrastructure.http.{HttpModule, Response}
import ru.dreamteam.infrastructure.MainTask
import sttp.tapir.{endpoint, Endpoint}
import sttp.tapir.generic.auto._
import sttp.tapir.json.tethysjson.jsonBody
import sttp.tapir.server.http4s.{Http4sServerInterpreter, Http4sServerOptions}
import zio._
import ru.dreamteam.business.handlers.user.handlers._
import ru.dreamteam.business.services.session.SessionService
import ru.dreamteam.business.services.users.UserService
import zio.interop.catz._
import zio.interop.catz.implicits._
import sttp.tapir.codec.newtype._

class UserModule(userService: UserService[MainTask])(
  implicit
  runtime: zio.Runtime[Unit],
  sessionService: SessionService[MainTask]
) extends HttpModule[Task] {

  val personalInfoEndpoint = endpoint
    .get
    .in("personal_info")
    .out(jsonBody[Response[PersonalInfoResponse]])
    .summary("Информация по пользователю")
    .handleWithAuthorization(_ =>
      userId => PersonalInfoHandler(userService)(PersonalInfoRequest(), userId)
    )

  val registrationEndpoint = endpoint
    .post
    .in("register")
    .in(jsonBody[RegistrationRequest])
    .out(jsonBody[Response[RegistrationResponse]])
    .summary("Регистрация пользователя")
    .description("Пользователь регистрируется, создавая логин и пароль")
    .handle(RegistrationHandler(userService)(_))

  val loginEndpoint = endpoint
    .post
    .in("login")
    .in(jsonBody[LoginRequest])
    .out(jsonBody[Response[LoginResponse]])
    .summary("Вход пользователя")
    .description("Пользователь логинится, указывая свои логин и пароль")
    .handle(LoginHandler(userService))

  override def httpRoutes(
    implicit
    serverOptions: Http4sServerOptions[Task]
  ): HttpRoutes[Task] = Http4sServerInterpreter.toRoutes(List(
    personalInfoEndpoint,
    registrationEndpoint,
    loginEndpoint
  ))

  override def endPoints: List[Endpoint[_, Unit, _, _]] =
    List(personalInfoEndpoint.endpoint, registrationEndpoint.endpoint, loginEndpoint.endpoint)

}
