package ru.dreamteam.infrastructure.utils

import cats.Applicative
import cats.effect.{IO, Sync}
import cats.implicits.catsSyntaxApplicativeId

import java.security.MessageDigest._
import scala.util.Random

trait SHA256Hash[F[_]] {
  def generate(text: String): F[String]

  def generateRandomSalt: F[String]
}

class SHA256HashImpl[F[_]: Sync] extends SHA256Hash[F] {
  private val random = new Random()

  override def generate(text: String): F[String] = Sync[F].delay(String.format(
    "%064x",
    new java.math.BigInteger(
      1,
      getInstance("SHA-256")
        .digest(text.getBytes("UTF-8"))
    )
  ))

  override def generateRandomSalt: F[String] = Sync[F].delay(random.alphanumeric.take(10).mkString)
}
