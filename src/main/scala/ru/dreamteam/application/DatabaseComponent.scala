package ru.dreamteam.application

import cats.effect.{Async, Blocker, ContextShift, Resource}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie.ExecutionContexts
import doobie.hikari.HikariTransactor
import doobie.util.transactor.Transactor
import doobie._

case class DatabaseComponent[F[_]](transactor: Transactor[F])

object DatabaseComponent {

  def build[F[_]: Async: ContextShift](dbConfig: DBConfig): Resource[F, DatabaseComponent[F]] = {
    val dataSource = new HikariDataSource()
    dataSource.setJdbcUrl(dbConfig.url)
    dataSource.setPassword(dbConfig.password)
    dataSource.setUsername(dbConfig.user)
    dataSource.setDriverClassName(dbConfig.driver)
    for {
      ec <- ExecutionContexts.fixedThreadPool(8)
      ce <- ExecutionContexts.cachedThreadPool
      xa = HikariTransactor[F](dataSource, ec, Blocker.liftExecutionContext(ce))
    } yield DatabaseComponent(xa)
  }

}
