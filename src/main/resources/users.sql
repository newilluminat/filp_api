
CREATE TABLE users (
    id uuid DEFAULT gen_random_uuid() PRIMARY Key,
    login varchar(32) UNIQUE,
    hash varchar(64),
    salt varchar(10)
);
